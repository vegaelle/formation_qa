from unittest import mock

from django.test import TestCase, Client
from django.http import HttpRequest, HttpResponse
from django.urls import reverse

from pyquery import PyQuery as pq

from .models import Question, Answer
from . import views
from . import forms


class QATestCase(TestCase):

    fixtures = ['qa_app/fixtures/test_data.yaml']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.client = Client()

    # def setUp(self):
    #     print('On appelle la méthode setUp')
    #
    # def tearDown(self):
    #     print('On appelle la méthode tearDown')

    def test_create_question(self):
        q = Question(title='foo', text='bar')
        q.save()
        self.assertIsNotNone(q.id, msg='La sauvegarde du modèle n\'a pas donné d\'id')
        qs = Question.objects.all()
        self.assertEqual(qs.count(), 101, msg='La table question devrait contenir un enregistrement')

    def test_initial_table_count(self):
        qs = Question.objects.all()
        self.assertEqual(qs.count(), 100, msg='La table question devrait contenir les enregistrements initiaux')

    def test_question_str(self):
        q = Question(title='foo', text='bar')
        self.assertEqual(str(q), 'foo', msg='La représentation textuelle de l\'objet question est incorrecte')

    def test_homepage(self):
        req = HttpRequest()
        res = views.homepage(req)
        self.assertIsInstance(res, HttpResponse)
        content_type = res.get('content-type')
        self.assertEqual(content_type, 'text/html; charset=utf-8')

    def test_homepage_post_question(self):
        req = HttpRequest()
        req.method = 'POST'
        req.POST = {'title': 'foo', 'text': 'bar'}
        with mock.patch('django.contrib.messages.success') as success:
            res = views.homepage(req)
            success.assert_called_once()
        self.assertEqual(res.status_code, 302, msg='La soumission du formulaire n\'a pas produit de redirection')
        location = res.get('location')
        self.assertEqual(location, reverse('homepage'), msg='La redirection ne s\'est pas faite sur la page d\'accueil')
        q = Question.objects.filter(title='foo')
        self.assertEqual(q.count(), 1, msg='La soumission du formulaire n\'a pas créé de question')


    def test_homepage_post_question_with_empty_field(self):
        req = HttpRequest()
        req.method = 'POST'
        req.POST = {'title': 'foo', 'text': ''}
        res = views.homepage(req)
        self.assertEqual(res.status_code, 200, msg='La soumission du formulaire a produit une redirection alors que le formulaire est incorrect')
        q = Question.objects.filter(title='foo')
        self.assertEqual(q.count(), 0, msg='La soumission du formulaire a créé une question')

    def test_homepage_functional(self):
        res = self.client.get(reverse('homepage'))
        self.assertEqual(res.status_code, 200)
        self.assertIn('form', res.context)
        self.assertIsInstance(res.context['form'], forms.QuestionForm)
        self.assertIn('questions', res.context)
        self.assertEqual(res.context['questions'].count(), 10, msg='La page d\'accueil n\'affiche pas 10 questions (pas de pagination ?)')

    def test_homepage_post_question_functional(self):
        res = self.client.post(reverse('homepage'), {'title': 'foo', 'text': 'bar'}, follow=True)
        self.assertEqual(res.status_code, 200)
        q = Question.objects.filter(title='foo')
        self.assertEqual(q.count(), 1, msg='La soumission du formulaire n\'a pas créé de question')
        # self.assertIn('Votre question a bien été posée'.encode(), res.content)
        doc = pq(res.content)
        alert = doc('div.alert.alert-success')
        self.assertEqual(len(alert), 1, msg='Aucun message de succès n\'est affiché sur la page')
        self.assertIn('Votre question a bien été posée', alert.text())
